#!/bin/sh

# flatpak run --command=sh org.kicad.KiCad

PATH="$XDG_DATA_HOME/python/bin:$PATH" PYTHONUSERBASE="$XDG_DATA_HOME/python" pip3 install kikit
PATH="$XDG_DATA_HOME/python/bin:$PATH" PYTHONUSERBASE="$XGG_DATA_HOME/python" python -m kikit.ui fab jlcpcb ./rework-ctf.kicad_pcb --assembly --schematic ./rework-ctf.kicad_sch  --no-drc --field JLCPCB --nametemplate ReworkCTF_v2_i0 exports/jlc/
