from machine import Pin, I2C, PWM
from challenges import get_all_challanges
import utime

# init i2c bus, scan for bus expander
i2c = I2C(1, scl=Pin(27), sda=Pin(26), freq=400_000)


status_pin = PWM(Pin(25))
status_pin.duty_u16(320)
status_pin.freq(10)


def rainbow():
    led_rainbow = [
        Pin(17, Pin.OUT),
        Pin(16, Pin.OUT),
        Pin(18, Pin.OUT),
        Pin(19, Pin.OUT),
        Pin(20, Pin.OUT),
        Pin(21, Pin.OUT),
        Pin(22, Pin.OUT),
        Pin(23, Pin.OUT),
    ]
    # blink the LEDs
    p_prev = led_rainbow[-1]
    delay = 10
    for p in led_rainbow:
        # blend LED on time
        p.on()
        utime.sleep_ms(delay * 3)
        p_prev.off()
        p_prev = p
        utime.sleep_ms(delay * 9)


# load challenges
challenge_list = []
for ch_name, challenge in sorted(get_all_challanges().items()):
    challenge_list.append((ch_name, challenge.Ch(Pin, i2c)))

while True:
    print("\n\nChecking all challenges...")
    check = True
    for name, obj in challenge_list:
        solved = obj.solved()
        print(f"Challenge {name} -  {obj.text:<73}: {solved}")
        check = check and solved
        obj.reset()
    if check:
        # all checks passed
        break
    utime.sleep_ms(2000)

while True:
    rainbow()
