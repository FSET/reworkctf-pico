def get_all_challanges():
    # without this delayed import, the rules could not import our sub-modules (e.g. `rule`)
    from . import C01, C02, C06, C11

    return {
        "C01": C01,
        "C02": C02,
        "C06": C06,
        "C11": C11,
    }
