from challenges.base import BaseChallenge


class Ch(BaseChallenge):
    """Check if GPIO16 and GPIO17 are shorted"""

    text = "short between GPIO16 and GPIO17 is cleared?"

    def reset(self):
        p16 = self.pin(16, self.pin.IN, self.pin.PULL_DOWN)
        p17 = self.pin(17, self.pin.IN, self.pin.PULL_DOWN)

    def solved(self) -> bool:
        """
        Checks if the Challange has been solved
        """
        p16 = self.pin(16, self.pin.OUT, value=0)
        p17 = self.pin(17, self.pin.IN, self.pin.PULL_DOWN)

        p16.on()
        if p17.value() == 1:
            # when switching p16 has influence on 17, that means
            # the pins are still shorted
            return False

        return True
