from challenges.base import BaseChallenge


class Ch(BaseChallenge):
    text = "Port expander is on the i2c-bus"

    def solved(self) -> bool:
        """
        Checks if the Challange has been solved
        """

        i2c_devices = self.i2c.scan()
        if 0x20 in i2c_devices or 0x22 in i2c_devices:
            return True
        return False
