from challenges.base import BaseChallenge


class Ch(BaseChallenge):
    """Check if GPIO18 and GPIO19 are shorted"""

    text = "short between GPIO18 and GPIO19 is cleared?"

    def reset(self):
        p18 = self.pin(18, self.pin.IN, self.pin.PULL_DOWN)
        p19 = self.pin(19, self.pin.IN, self.pin.PULL_DOWN)

    def solved(self) -> bool:
        """
        Checks if the Challange has been solved
        """
        p18 = self.pin(18, self.pin.OUT, value=0)
        p19 = self.pin(19, self.pin.IN, self.pin.PULL_DOWN)

        p18.on()
        if p19.value() == 1:
            # when switching p18 has influence on 19, that means
            # the pins are still shorted
            return False

        return True
