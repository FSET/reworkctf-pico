class BaseChallenge:
    """A base class to represent a challenge"""

    def reset(self):
        pass

    def __init__(self, pin, i2c):
        self.pin = pin
        self.i2c = i2c
