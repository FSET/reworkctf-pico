Challenges Help
====
# Project info
The goal of `reworkCTF-pico` is to teach the skills needed to repair or rework a circuit board (PCB). The intended audience is people that are already familiar with the basics of soldering, looking to improve their skills.
The challenges try to mimic common tasks encountered when repairing a PCB, like removing/placing components and cutting/making connections. This document descripes all the challenges in detail: what they are, which tools you need to solven them and where to start.

If you want a harder challenge, don't look at this document. Instead open the `kicad_pcb` file and figure everything out there. The locations of the challenges can be found on the `User.Comments` layer.

## Design considerations

### Goals
The original intention of this project is to be accessible and cheap. Also, it should create minimal e-waste.
* All solder-challenges can be solved with just a soldering iron, no Hot-Air required. You are however free to use a hot-air-station.
* Parts used are rather common and were chosen for easy drop-in-replacement should something be out of stock
* 4-Layer layout without fancy process requirements
* No programmer is needed to upload any software
* The challenge-part and the microcontroller part are independent and can be separated

In reality, to solve the challanges, you will need quite a lot of tools. Most of those tools are not that expensive and I would argue that every hobby electronics workshop should have them. The biggest show stopper is the microscope. If you can't acces one, a smartphone is actually a quite decent tool for magnification. Just prop it up somewhere (e.g. on a cup) so it sits securely about 10cm above the PCB. Use the zoom-function of the camera-app to get the magnification required for the job.

### Rules
None. This is not a competition. I was hesitant to even add the points one gets for solving a challenge. Instead, think of those points as the difficulty level [^1].
Feel free to share your success though. I would love to see pictures of solved challenges. Post them to mastodon with the hashtag `#ReworkCTF`.

[^1]: Since difficutly is highly subjective, you will most likely find challenges that feel too hard or too easy.

### Tools


### Optical magnification


# List of Challenges
| ID          | Name                                | Points   |
| ----------- | ----------------------------------- | -------- |
| [C00](#c00-missing-trace) | missing trace                       | 50       |
| [C01](#c01c02-shorted-traces-) | shorted traces                      | 75       |
| [C02](#c01c02-shorted-traces-) | shorted traces with a bend          | 100      |
| [C03](#c03-wrong-resistor-value-) | resistor R17 has the wrong value    | 50       |
| [C04](#c04-missing-part-) | missing 0201 part (R19)    | 100       |
| [C05](#c05-soldermask-not-removed-) | missing soldermask aperture    | 100       |
| [C06](#c06-wrong-i2c-address-) | wrong I2C address | 100    |
| [C07](#c07-short-between-pads) | short between pads | 50   |
| [C08](#c08-missing-pad) | missing pad | 50    |
| [C09](#c09-missing-trace-pin7-u6) | missing trace (Pin7, U6) | 100    |
| [C10](#c10-hole-%EF%B8%8F-diameter-to-small) | hole diameter to small | 100    |
| [C11](#c11-wrong-footprint-part-missing) | wrong footprint, part missing | 300   |
| [C12](#c12-missing-soldermask) | missing soldermask | 100    |
| [C13](#c13-sdascl-swapped) | SDA/SCL swapped | 200    |
| [C14](#c14-missing-decouling-caps) | missing decouling caps | 100    |
| [C15](#c15-part-in-wrong-orientation) | part in wrong orientation | 100    |
| [C16](#c16-missing-pullups) | missing pullups | 100    |
| [C17](#c17-blobhaj-on-the-back-of-the-pcb-has-a-new-tip-on-the-soldering-iron-tin-it) | Add tin to a pad | 50    |
| [C18](#c18-secret-) | Find the secret | 0   |

![](pictures/overview.png)




# Detailed description
## C00 missing trace
The connection from the pico-board to the resistor R22 is missing.
Fortunately there are some pads we can use to create that missing connection. They are located between the pins 29 and 30 of the pico board. However, those pads are rather small (0.4x0.4mm²).


### Tools & Materials
* Soldering iron
* Fresh tin, flux
* Thin wire

![](pictures/c00.png)
<details> <summary>Hint 1</summary>
    
* Tin the pads and wire first
* Flux everything
</details>
<details> <summary>Hint 2</summary>

* Take a single strand from a fine-stranded wire, seperate it from the bunch, but don't cut it off yet.
* Instead solder the wire down first, then cut the leftover length you don't need
</details>
<details> <summary>Hint 3</summary>

* Put kapton tape on the nearby ENIG pads to prevent any solder sticking to it.
</details>
<details> <summary>Solution image</summary>

![](pictures/c00_sol.png)
</details>

## C01/C02 shorted traces 🔪
Nets have been accidentally shorted. You need to clear that short. 
* Between GPIO18 and GPIO19, left of R17
* Between GPIO16 and GPIO17, below R15

### Tools & Materials
* Knife
* Solder mask

![](pictures/c01.png)

<details> <summary>Hint 1</summary>

* make two cuts and remove the copper inbetween
* start with a scraping pass without much pressure
* with each pass, increase the pressure and cut depth
</details>
<details> <summary>Solution image</summary>

![](pictures/c01_sol.png)

</details>

## C03 Wrong resistor value 🧮
Resistor R17 has the wrong value 2.2k, but it should be 270R.
To fix this, solder a 330R resistor piggyback on the existing resistor. The 330R resistor can be found in the spare area in the top right of the board.
When done, use a multimeter to check if the fix was successful.

330R parallel to 2.2k is 
$R = \frac{1}{\frac{1}{330\Omega} + \frac{1}{2200\Omega}}  = 287\Omega$

### Tools & Materials
* Soldering iron
* flux, tin
* tweezers
* Multimeter

<details> <summary>Hint 1 - Removing resistors or capacitors</summary>

* make sure you have good access to the 330R resistor by removing the capacitor next to it first
* using a big tip, removing small SMD parts (eg. 0603 Resistor or Capacitor) can be done by touchting both sides of the parts at the same time. Add additional solder if the thermal contact is not good.
* this technique works with all parts where your tip is big enough
</details>


<details> <summary>Hint 2 - SMD resistor codes</summary>

* `222` evaluates to $22 * 10^{2} = 2200\Omega$
* `331` evaluates to $33 * 10^{1} = 330\Omega$
</details>


<details> <summary>Solution image</summary>

![](pictures/c03.png)

</details>

## C04 Missing part ❌
A rather small 0201 resistor (R19) is missing. Take one from the spare area.

<details> <summary>Hint 1 - Placing small parts</summary>

* the 0201 part is really small, it is easy to heat both ends at the same time with the tip of your iron and pick it up. Add solder to both ends to get good thermal conductivity
* the hard part is to not lose track of the part once its 'swimming' in the puddle of tin at the top of the tip
* with enough flux, you can just place this puddle of tin with the resistor down in the right spot. Surface tension might be enough to keep the resistor on its pads. If not, put the tip of your tweezers or any other pointy thing on top of the 0201 chip-resistor to keep it in place while you remove the soldering iron.


Watch this [youtube video](https://www.youtube.com/watch?v=8JM4oCpWnjU) where the removal part is shown.
    
</details>

## C05 soldermask not removed 🪛
Resistor R20 could not be placed. There is soldermask on the right pad, making it impossible to solder it down.
Remove the soldermask, then place the resistor. A 270R resistor can be found in the spare area.

### Tools & Materials
* Soldering iron
* flux, tin
* scraper
* tweezers

![](pictures/c05.png)

<details> <summary>Hint 1</summary>

* don't use a sharp knife for scraping away the mask
* the tip of a small screwdriver is great for this kind of work, or any other tool with a edge that is neither sharp not blunt    
</details>

## C06 wrong I2C address 📧
**Hint** If you want to make this harder, solve this challenge after [C11](#c11-wrong-footprint-part-missing).

The address of the I2C busexpander is set to 0x20. Unfortunately that does not match the value used in the software (0x22). Since software is really hard to change, we will fix this in hardware instead.
Figure out how to change the address and make it work.

![](pictures/c06.png)

<details> <summary>Hint 1 - I²C Addresses</summary>

* Change Pin2 from GND to +3.3V to set the 0x22 address
</details>
<details> <summary>Hint 2</summary>

* Cut the 4 thermal spokes to isolate the pad
* Or, lift Pin2 while soldering `U6` down
* Run a wire from Pin16 to Pin2
</details>
<details> <summary>Solution image</summary>

![](pictures/c06_sol.png)

</details>

## C07 short between pads
**Hint** If you want to make this harder, solve this challenge after [C11](#c11-wrong-footprint-part-missing).

Pin16 and Pin15 are shorted. With SCL pulled up high like this, the I²C connection cannot work. Clear this short.

![](pictures/c07.png)

## C08 missing pad
A pad for Pin4 of `U6` is missing. There is a simple solution for this, just scrape away a few millimeters of soldermask on the respective trace and connect it to Pin4 with a big blob of solder or a small piece of wire.

![](pictures/c08.png)

<details> <summary>Bonus Points</summary>

Instead of choosing the easy solution, do it the hard way: place a new pad.

* Rework the ground plane into a new pad
* Cut a matching piece of copper tape to complete the pad
* Glue the pad down, connect it to the trace
* Add solder-mask to clean it up
</details>
<details> <summary>Solution image</summary>

![](pictures/c08_sol.png)
</details>

## C09 missing trace (Pin7, U6)
Pin7 of `U6` should connect to Pin5 of J3. Since we might want to fit a pinheader on the top layer, soldering to Pin5 from the top is discouraged.
![](pictures/c09.png)

<details> <summary>Hint 1</summary>

There is a via directly below Pin7. 
* You can use that to route the signal to the bottom layer.
* The via is filled, use a drill to clean it (only applies to older revisions of the board)
* Thread a enamel wire through that via
</details>
<details> <summary>Bonus Points</summary>

Don't use a wire on top, build a track that connects Pin7 to the via instead. Then create a new connection on the bottom layer from that via to Pin5 of J3.
</details>


## C10 hole 🕳️ diameter to small
* Newer boards (handed out since GPN21 in 2024)
  * A normal 0.64mm square pin won't fit into the hole of the square pad. You either have to modify the pin or the hole
  * Pins: Use a file or sharp knife to remove material
  * Pins: you can remove one of the metal pins from the pin-strip by pulling it out with pliers
  * Hole: Enlargen it with a drill, make sure to drill off center. If you drill exactly in the center, you will remove all the internal metalization and thus break connectivity.
  * Brute-Force: With the proper tools, you might be able to press the pin into the hole. That is actually a well established method for mounting connectors called "pressfit".
* First iteration of boards
   * actually, the diameter is just big enough to fit a THT without force
   * consider this a defect of the defect^^
   * next iteration of the board will make that hole even smaller


## C11 wrong footprint 🦶, part missing
Component `U6` is missing. The footprint does not match the part, thus it could not be soldered down. You have to fix that.

![](pictures/c11.png)


<details> <summary>Hint 1 - Removal</summary>

There are multiple options on how to remove a SOIC part
  * If you have access to a hot-air station, use that. The parts next it might fly away, prevent that by covring them. Aluminium foil or kapton tape works for that.
  * Use the method with a 'heatspreader' as shown in this [youtube video](https://www.youtube.com/watch?v=qObbANkrmUM)
  * Add low-temperature solder to make it easier to heat all pins at the same time
</details>
<details> <summary>Hint 2</summary>

![](pictures/c11_hint2.png)
  * The part is to big, bend the leads back below the component (j-leads)    
  * Prior to the bending, remove as much leftover solder from the part as possible
  * You can bend all pins at once by pressing/rolling the part on a hard surface
  * After forming the j-leads, take some time to clean up the bends and make sure all pins are nice and parallel
</details>

<details> <summary>Hint 3 - Soldering</summary>

  * Start by tacking the component down with just one pad. 
  * Pin9 is the best choice for that since it has low thermal mass.
  * Align the part while reflowing that pin until you are happy with the placement, then solder the remaining pins.
</details>

## C12 missing soldermask
Cover the area with soldermask, epoxy or something else suited for the job. While doing this, you can also touch up other areas of the board that got damaged from previous rework.


## C13 SDA/SCL swapped
The `SCL` and `SDA` lines got swapped in the schematic. You need to cut some traces and reconnect them to fix this.
There are multiple places where you can do this repair. Select one 

<details> <summary>Hint 1 - Locations</summary>

![](pictures/c13.png)
  * Left of `Q2` and `Q3`. Cut the traces between GPIO Pins31/32 and Pin3 of the FETs.
  * Right of `R23` and `R24`. 
    * Cut the traces and use the vias (as done in the solution image)
    * Drill out the vias, scrape some soldermask and recreate the connections to Pin14/15 of `U6`.
</details>

<details> <summary>Bonus Points +200</summary>

Do the repair on the inner layers, don't change any traces/vias on the outer layers.
**Warning** This is really hard. Only attempt this if you have proper milling tools.
</details>

## C14 missing decoupling caps
It is good practice to add a decoupling capacitor to the supply pins as shown in the [Datasheet](https://www.ti.com/lit/ds/symlink/tca9534.pdf#page=29). Unfortunately the PCB designer did not read that datasheet, thus the decoupling cap is missing.
Take the capacitor from the spare parts area and attach it to Pin16.

<details> <summary>Hint 1</summary>

  * The LED `D7` has a GND pad nearby. You can mount the capacitor between that pad and Pad16 of `U6`.
</details>
<details> <summary>Bonus Points</summary>

Don't use an existing pad, but scrape away soldermask and cut thermal spokes to create a new pad.
</details>
<details> <summary>Solution image</summary>
    
![](pictures/c14_sol.png)
</details>


## C15 part in wrong orientation
The LED `D9` was mounted in the wrong orientation. You need to rotate it by 180°.

<details> <summary>Hint 1 </summary>

  * remove nearby parts (`D10`) to get better access
  * with a big tip, you can touch both terminals of a 0805 led at the same time
</details>


## C16 missing pullups
The I²C Bus needs pullups to work properly. You need to add them. As always, fetch the resistors from the spare parts area on the top right. Study the layout and schematic to find a good spot for the resistors.

![](pictures/c16.png)

<details> <summary>Hint 1 - Resistor location</summary>

  * place the resistors directly on the terminal of Q3/Q2 (Pin3)
</details>

<details> <summary>Solution image</summary>
    
![](pictures/c16_sol.png)
</details>


## C17 blobhaj 🦈 on the back of the PCB has a new tip on the soldering iron, tin it
   - this challenge is a good indicator on how much power your soldering iron has
   - the 'tip-pad' does not have thermal spokes and will sink lots of heat
   - with a weak solding iron, it might be near impossible to wet the pad with solder
![](pictures/c17.png)


## C18 Secret 🥷
There is a something hidden that is not documented. Its neither in the software-sourcecode nor in the kicad-files on git.
If you find it, you will know what to do.

