## Original by [cpresser](https://gitlab.aachen.ccc.de/cpresser/reworkctf-pico)
Fork by FSET. Some components and footprints are changed to reduce costs when using JLCPCBs assembly service.

[Original repo](https://gitlab.aachen.ccc.de/cpresser/reworkctf-pico) forked @[ad3e1207](https://gitlab.aachen.ccc.de/cpresser/reworkctf-pico/-/tree/ad3e120758efea0da5997b5ba7cf9e84364ce87f).

# ReworkCFT Pico
Features:
* Base-board is a 1:1 compatible RaspberryPi Pico
  * USB-C instead of Micro-USB
  * SPI Flash in SO-8 with more capacity
  * larger SMT parts for easier assembly/rework
* Challenges
  * 17 rework challenges
  * Challenge part of the board can be broken off
  * when you don't break it of, you will get a useful 5V 8-Bit IO
  * additional parts required to solve a challenge are already on the board. Unsolder them and put the into the correct location.
* The project tries to be accessible
  * All solder-challenges can be solved with just a soldering iron, no Hot-Air required
  * Parts used are rather common and were chose for easy drop-in-replacement should something be out of stock
  * 4-Layer layout without fancy process requirements

![3D-Preview](/exports/3d-preview.png)

# Tools overview
## Mandatory
* soldering iron with a decent tip (don't go to smol, a 2mm cone can do all the challenges)
* lots of flux
* pointy things, tweezers, knifes, blades
* magnification
  * jewelers eyepiece
  * microscope
* UV-Curablable soldermask (only used for one challange). Or some kind of epoxy/whatever to solve the 'missing soldermask' challenge
* fresh lead-free solder
* different kinds of wire
* multimeter with probes

## Useful
* stuff to clean flux (brushes, isopropyl alchohol)
* smol springloaded multimeter probes
* even more pointy sharp things for scraping and cutting
* more flux, different types
* low temperature solder

## Not required
* CNC-Mill
* Hot-Air Station

# Challenges
[List of challenges](/docs/Challenges.md) including hints and solutions.


# Software
The RP2040 has a ROM bootloader, so you don't need JTAG or SWD for initial programming.

To install micropython, connect the board and copy the .uf2 file to the drive. Then power-cycle.
Now the board should enumerate as CDC-ACM device. Connect to that and you will get a python shell.

I recommend to use `rshell` to upload code to the board.


## Example rshell usage
```bash
# to to the local software directory
cd software
# install rshell with pip: pip3 install rshell
rshell -p /dev/ttyACM0

# inside rshell, copy the files to the board, then enter repl
cp main.py /pyboard/
cp -r challenges/ /pyboard/
repl

# in repl, import the main module
import main
```
## Software under Windows 

For Windows Thonny IDE is a good possibillity to upload python files to the raspberry pi and test the board. 

Go to  Tools -> Options -> Interpreter and select MicroPython(Raspberry Pi Pico) and select the port of the Raspberry Pi Pico / ReworkCTF Board. 

Go to View -> Files and then you can see the files on the local machine and the raspberry pi pico 

you can start a programm from the pico, via the shell, for example the main.py which checks the status of some challenges via "import main" 


## Resources
* https://www.raspberrypi.com/documentation/microcontrollers/micropython.html
* https://docs.micropython.org/en/latest/rp2/quickref.html
* https://github.com/dhylands/rshell


## Legal
**NoAI**: This project may not be used in datasets for, in the development of, or as inputs to generative AI programs.

**§44b**: Dies ist ein in maschinenlesbarer Form vorliegender Nutzungsvorbehalt entsprechend §44b UrhG

**License**: [CERN-OHL-S](LICENSE)
* 2023 cpresser
* 2024 Erik Wichmann, Simon Prein, Sophie Schüttpelz
